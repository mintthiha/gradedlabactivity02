//Thiha Min Thein, 2245136

package applicationpackage;

import vehiclespackage.Bicycle;

public class BikeStore {
    public static void main(String args[]){
        Bicycle bike1 = new Bicycle("Giant Bicycles", 4, 40);
        Bicycle bike2 = new Bicycle("Bianchi", 7, 60);
        Bicycle bike3 = new Bicycle("Trek", 3, 45);
        Bicycle bike4 = new Bicycle("Kona", 5, 50);
        Bicycle[] bikes = {bike1, bike2, bike3, bike4};
        
        for(int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
        }
    
    }
    
    
}
